from aiogram import Dispatcher, types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text


async def cmd_start(message: types.Message, state: FSMContext):
    await state.finish()
    await message.answer(
        """Здрасте, меня зовут Top Tyan, а вот мой создатель: @I_Dart_I.
Правда красавчик?
Для того что бы узанть что я умею выберите команду /help""", reply_markup=types.ReplyKeyboardRemove())


async def cmd_cancel(message: types.Message, state: FSMContext):
    await state.finish()
    await message.answer("Действие отменено", reply_markup=types.ReplyKeyboardRemove())

async def cmd_help(message: types.Message, state: FSMContext):
    await state.finish()
    await message.answer("""Я могу показать вам
Гороскоп - /goroskop или
найти для вас отель - /serchhotel?""", reply_markup=types.ReplyKeyboardRemove())


def register_handlers_common(dp: Dispatcher):
    dp.register_message_handler(cmd_start, commands="start", state="*")
    dp.register_message_handler(cmd_start, Text(equals="привет", ignore_case=True), state="*")
    dp.register_message_handler(cmd_cancel, commands="cancel", state="*")
    dp.register_message_handler(cmd_cancel, Text(equals="отмена", ignore_case=True), state="*")
    dp.register_message_handler(cmd_help, commands="help", state= "*")
