import logging
import asyncio

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.types import BotCommand
from aiogram.contrib.fsm_storage.memory import MemoryStorage


from common import register_handlers_common
from hotels import register_handlers_hotels
from goroskop import register_handlers_goroskop
logger = logging.getLogger(__name__)

async def set_commands(bot: Bot):
  commands = [
    BotCommand(command="/serchhotel", description="поиск отелей"),
    BotCommand(command="/goroskop", description="гороскоп"),
    BotCommand(command="/cancel", description="Отменить текущее действие"),
    BotCommand(command="/help", description= "Показать что умею")
  ]
  await bot.set_my_commands(commands)

async def text(message: types.Message):
  await message.answer("""Не ломай меня пожалуйста,
просто введи /help и будет тебе счастье ;)""")

async def main():

  logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
  )
  logger.error("Starting bot")



  # Объявление и инициализация объектов бота и диспетчера
  bot = Bot(token="5318677080:AAFkUoal6iG8GuciW3lF8ardU6nJPysd6j0")
  dp = Dispatcher(bot, storage=MemoryStorage())

  register_handlers_common(dp)
  register_handlers_hotels(dp)
  register_handlers_goroskop(dp)
  await set_commands(bot)

    # Запуск поллинга
    # await dp.skip_updates()  # пропуск накопившихся апдейтов (необязательно)
  await dp.start_polling()


if __name__ == '__main__':
    asyncio.run(main())



