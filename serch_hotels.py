import requests
import json


def serchID(city):
  url = "https://hotels4.p.rapidapi.com/locations/v2/search"
  querystring = {"query": city,"locale":"en_US","currency":"USD"}
  headers = {
  	"X-RapidAPI-Host": "hotels4.p.rapidapi.com",
  	"X-RapidAPI-Key": "d0cf243be7msh5815073ca85e22dp15f1a6jsnce52eba8762b"
  }

  response_1 = requests.request("GET", url, headers=headers, params=querystring)
  data = json.loads(response_1.text)

  for i in data["suggestions"]:
    if i["group"] == "CITY_GROUP":
      for j in i["entities"]:
        if j["name"].lower() == city.lower() and j["type"] == "CITY":
          return j["destinationId"]
          break

"""Поис отелей в данном городе"""
def serch_hotels(city_id, sorted_price, numbers):
  price = ""
  list_hotel = []
  list_hotel_id = []
  if sorted_price == "по убыванию цены":
    price = "PRICE"
  elif sorted_price == "по взрастонию цены":
    price = "PRICE_HIGHEST_FIRST"

  url = "https://hotels4.p.rapidapi.com/properties/list"
  querystring = {
    "destinationId":city_id, "pageNumber":"1","pageSize": numbers,"checkIn":"2021-05-01",
    "checkOut":"2021-05-15","adults1":"1","sortOrder": price, "locale":"moskow","currency":"RUB"}
  headers = {
    "X-RapidAPI-Host": "hotels4.p.rapidapi.com",
    "X-RapidAPI-Key": "d0cf243be7msh5815073ca85e22dp15f1a6jsnce52eba8762b"
  }

  response = requests.request("GET", url, headers=headers, params=querystring)
  data = json.loads(response.text)
  for i in data["data"]["body"]["searchResults"]["results"]:
    list_hotel.append(f"""Назание: {i["name"]}
Адрес: {i["address"]["streetAddress"]}
Цена: {i["ratePlan"]['price']['current']}.""")
    list_hotel_id.append(i["id"])
  return list_hotel, list_hotel_id

"""Поиск фотограйфий отеля"""

def foto(hotel_id, foto_numbers):
  list_foto = []
  url = "https://hotels4.p.rapidapi.com/properties/get-hotel-photos"

  querystring = {"id" : hotel_id}

  headers = {
      "X-RapidAPI-Host": "hotels4.p.rapidapi.com",
      "X-RapidAPI-Key": "d0cf243be7msh5815073ca85e22dp15f1a6jsnce52eba8762b"
  }
  response = requests.request("GET", url, headers=headers, params=querystring)
  data = json.loads(response.text)
  n = 0
  for i in data["hotelImages"]:
    if n == int(foto_numbers):
      break
    else:
      list_foto.append(i["baseUrl"].replace("{size}", "z"))
    n += 1
  return list_foto
