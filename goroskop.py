import requests
from bs4 import BeautifulSoup

from aiogram import Dispatcher, types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup

def gorockop(znak):
  znaki = {"близнецы" : ["https://horo.mail.ru/prediction/gemini/today/", "https://horo.mail.ru/prediction/gemini/tomorrow/", "https://horo.mail.ru/prediction/gemini/week/"],
             "овен": ["https://horo.mail.ru/prediction/aries/today/", "https://horo.mail.ru/prediction/aries/tomorrow/", "https://horo.mail.ru/prediction/aries/week/"],
             "телец": ["https://horo.mail.ru/prediction/taurus/today/", "https://horo.mail.ru/prediction/taurus/tomorrow/", "https://horo.mail.ru/prediction/taurus/week/"],
             "рак": ["https://horo.mail.ru/prediction/cancer/today/", "https://horo.mail.ru/prediction/cancer/tomorrow/", "https://horo.mail.ru/prediction/cancer/week/"],
             "лев" : ["https://horo.mail.ru/prediction/leo/today/", "https://horo.mail.ru/prediction/leo/tomorrow/", "https://horo.mail.ru/prediction/leo/week/"],
             "дева" : ["https://horo.mail.ru/prediction/virgo/today/", "https://horo.mail.ru/prediction/virgo/tomorrow/", "https://horo.mail.ru/prediction/virgo/week/"],
             "весы" : ["https://horo.mail.ru/prediction/libra/today/", "https://horo.mail.ru/prediction/libra/tomorrow/", "https://horo.mail.ru/prediction/libra/week/"],
             "скорпион" : ["https://horo.mail.ru/prediction/scorpio/today/", "https://horo.mail.ru/prediction/scorpio/tomorrow/", "https://horo.mail.ru/prediction/scorpio/week/"],
             "стрелец" : ["https://horo.mail.ru/prediction/sagittarius/today/", "https://horo.mail.ru/prediction/sagittarius/tomorrow/", "https://horo.mail.ru/prediction/sagittarius/week/"],
             "козерог" : ["https://horo.mail.ru/prediction/capricorn/today/", "https://horo.mail.ru/prediction/capricorn/tomorrow/", "https://horo.mail.ru/prediction/capricorn/week/"],
             "водолей" : ["https://horo.mail.ru/prediction/aquarius/today/", "https://horo.mail.ru/prediction/aquarius/tomorrow/", "https://horo.mail.ru/prediction/aquarius/week/"],
             "рыбы" : ["https://horo.mail.ru/prediction/pisces/today/", "https://horo.mail.ru/prediction/pisces/tomorrow/", "https://horo.mail.ru/prediction/pisces/week/"]
             }

  blok =[]
  for i in znaki.get(znak):
    response = requests.get(i).text
    soup = BeautifulSoup(response, 'lxml')
    blok.append(soup.find_all('p'))
  return blok


days = ["Сегодня:", "Завтра:", "На неделю:"]
znaki = ["близнецы", "овен", "телец","рак", "лев", "дева", "весы", "скорпион", "стрелец", "козерог", "водолей", "рыбы"]

class Goroskop(StatesGroup):
  waiting_for_znak =State()

async def start_goroskop(message: types.Message):
  keybord = types.ReplyKeyboardMarkup(resize_keyboard= True)
  for zz in znaki:
    keybord.insert(zz)
  await message.answer("Выберите из предложенного списка свой знак зодиака:", reply_markup=keybord)
  await Goroskop.waiting_for_znak.set()

async def rezaltt_goroskop(message: types.Message, state: FSMContext):
    if message.text not in  znaki:
      await message.answer("Подалуйста выберите из предложенных вариантов")
    else:
      n = 0
      await state.update_data(z_z = message.text)
      user = await state.get_data()
      rezalt = gorockop(user['z_z'])
      for i in rezalt:
        await message.answer(days[n], reply_markup=types.ReplyKeyboardRemove())
        await message.answer(i[0].text)
        await message.answer(i[1].text)
        n +=1
    await state.finish()
async def cmd_cancel(message: types.Message, state: FSMContext):
    await state.finish()
    await message.answer("Действие отменено", reply_markup=types.ReplyKeyboardRemove())

def register_handlers_goroskop(dp: Dispatcher):
    dp.register_message_handler(start_goroskop, commands= "goroskop", state="*")
    dp.register_message_handler(rezaltt_goroskop, state=Goroskop.waiting_for_znak)
    dp.register_message_handler(cmd_cancel, commands="cancel", state="*")
