from aiogram import Dispatcher, types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup

from serch_hotels import serch_hotels, serchID, foto

class Hotels(StatesGroup):
  waiting_for_price = State()
  waiting_for_city = State()
  waiting_for_number_hotles = State()
  waiting_for_foto = State()
  waiting_for_number_fotes = State()


async def price(message: types.Message):
  keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
  btn = "по возрастанию цены"
  btn2 = "по убыванию цены"
  keyboard.add(btn, btn2)
  await message.answer("""И так я могу помочь найти вам отели для этого просто выберите:
 По взрастнию цены
 или по убыванию цены""", reply_markup=keyboard)
  await Hotels.waiting_for_price.set()

async def city(message: types.Message, state: FSMContext):
  if message.text.lower() == "по возрастанию цены" or  message.text.lower() == "по убыванию цены":
    await state.update_data(price=message.text.lower())
    await Hotels.next()
    await message.answer("""Теперь введите назавние города(по английски пожалуйста): """, reply_markup =types.ReplyKeyboardRemove())
  else:
    await message.answer("""Пожaлуйста выберите
По возрастонию цены
По убыванию цены""")
    return

async def numbers_hotels(message: types.Message, state: FSMContext):
  await state.update_data(city = message.text.lower())
  await Hotels.next()
  await message.answer("Выберите кооличество отелей но не больше 10 штук")

async def foto_hotels(message: types.Message, state: FSMContext):
  try:
     if int(message.text) > 10:
       await message.answer("Вы ввели слишком много отелей давайте еще раз.")
       return
     else:
       keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
       btn1 = "да"
       btn2 = "нет"
       keyboard.add(btn1, btn2)
       await state.update_data(numbers = message.text)
       await Hotels.next()
       await message.answer("Хотите получать фотографии отелей?", reply_markup=keyboard)
  except:
    await message.answer("""Вы ввели буквы ая-яй.
Давайте еще раз""")

async def foto_and_hotels(message: types.Message, state: FSMContext):
  if message.text.lower() == "нет":
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add('Поехали')
    await state.update_data(foto=message.text)
    await Hotels.next()
    await message.answer("Сейчас найдем вам отели, но для этого напишете слово поехали)", reply_markup=keyboard)
  elif message.text.lower() == "да":
    await state.update_data(foto=message.text)
    await Hotels.next()
    await message.answer("Введите кооличество фотографий но небольше 5", reply_markup =types.ReplyKeyboardRemove())


async def num_foto(message: types.Message, state: FSMContext):
  await state.update_data(foto_numbers=message.text)
  user = await state.get_data()
  if user['foto'].lower() == 'нет':
    user = await state.get_data()
    await message.answer("Сейчас найду для вас отели", reply_markup =types.ReplyKeyboardRemove())
    city_id = serchID(user['city'])
    list_hotel = serch_hotels(city_id=city_id, sorted_price=user['price'], numbers=user['numbers'])[0]
    for i in range(int(user['numbers'])):
      await message.answer(list_hotel[i])


  else:
    try:
      if int(message.text) >5:
        await message.answer("Вы ввели слишком много. Давайте повторим")
        return
      else:
        try:
          user = await state.get_data()
          city_id = serchID(user['city'])
          list_hotel = serch_hotels(city_id=city_id, sorted_price=user['price'], numbers = user['numbers'])[0]
          hotel_id = serch_hotels(city_id=city_id, sorted_price=user['price'], numbers= user['numbers'])[1]
          for i in range(int(user['numbers'])):
              await message.answer(list_hotel[i])
              foto_hotel = foto(hotel_id=hotel_id[i],foto_numbers = user['foto_numbers'])
              for fotos in foto_hotel:
                  await message.answer(fotos)
        except:
            await message.answer("У меня не получилсь давйте попробуем снова.")

    except:
          await message.answer("""Вы ввели букавы, ай-ай-ай
Давайте еще раз.""")
    await state.finish()
async def cmd_cancel(message: types.Message, state: FSMContext):
  await state.finish()
  await message.answer("Действие отменено", reply_markup=types.ReplyKeyboardRemove())




def register_handlers_hotels(dp: Dispatcher):
    dp.register_message_handler(price, commands= "serchhotel", state="*")
    dp.register_message_handler(city, state=Hotels.waiting_for_price)
    dp.register_message_handler(numbers_hotels, state=Hotels.waiting_for_city)
    dp.register_message_handler(foto_hotels, state=Hotels.waiting_for_number_hotles)
    dp.register_message_handler(foto_and_hotels, state=Hotels.waiting_for_foto)
    dp.register_message_handler(num_foto, state=Hotels.waiting_for_number_fotes)
    dp.register_message_handler(cmd_cancel, commands = "cancel", state = "*")

